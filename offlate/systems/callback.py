#   Copyright (c) 2020 Julien Lepiller <julien@lepiller.eu>
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU Affero General Public License as
#   published by the Free Software Foundation, either version 3 of the
#   License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU Affero General Public License for more details.
#
#   You should have received a copy of the GNU Affero General Public License
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.
####

from ..formats.callback import FormatCallback

class SystemCallback(FormatCallback):
    """
    The base class for callbacks used in the different systems.
    """
    def reportProgress(self, progress):
        """
        Report progress in some operation.  This method is called during download,
        update and upload of translation projects.
        """
        raise Exception("Unimplemented method in concrete class: reportProgress")

    def askPassword(self):
        """
        Get a password (interactively or not) and return it to the caller.
        """
        raise Exception("Unimplemented method in concrete class: askPassword")

    def githubBranchError(branch):
        """
        Report an error while accessing a branch
        """
        raise Exception("Unimplemented method in concrete class: githubBranchError")

    def gitlabBranchError(branch):
        """
        Report an error while accessing a branch
        """
        raise Exception("Unimplemented method in concrete class: gitlabBranchError")

    def gitlabTokenNotFoundError(server):
        """
        Report an error while accessing a gitlab server
        """
        raise Exception("Unimplemented method in concrete class: gitlabTokenNotFoundError")
