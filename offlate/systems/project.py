#   Copyright (c) 2020 Julien Lepiller <julien@lepiller.eu>
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU Affero General Public License as
#   published by the Free Software Foundation, either version 3 of the
#   License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU Affero General Public License for more details.
#
#   You should have received a copy of the GNU Affero General Public License
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.
####

class Project:
    """
    The base class for translation projects.

    A project is a class that represents a translation project the user is
    working on.  Each project corresponds to a specific system and has its
    own way of dealing with local projects and sending changes upstream.
    """
    def __init__(self, name, lang, conf, data = {}):
        self.name = name
        self.lang = lang
        self.conf = conf
        self.data = data

    @staticmethod
    def tr(s):
        """
        A no-op function that returns its only argument.  This is used so
        strings are detected by Qt for translation.
        """
        return s

    def open(self, basedir):
        """
        Operations done when opening the project from files.

        :param str basedir: The base directory in which files for this project
            are stored.
        :rtype: None
        """
        raise Exception("Unimplemented method in concrete class: open")

    def initialize(self, basedir, callback=None):
        """
        Operations done when creating a new project of this type.

        :param str basedir: The base directory in which files for this project
            are stored.
        :param SystemCallback callback: An optional callback that can be used
            for error handling and progress report.
        :rtype: None
        """
        raise Exception("Unimplemented method in concrete class: initialize")

    def update(self, callback):
        """
        Update the project.

        :param SystemCallback callback: A callback that is used for error
            handling and progress report.
        :rtype: None
        """
        raise Exception("Unimplemented method in concrete class: update")

    def send(self, callback):
        """
        Send the current state of the project upstream.  This is used to
        send the user's progress on the translation.

        :param SystemCallback callback: A callback that is used for error
            handling and progress report.
        :rtype: None
        """
        raise Exception("Unimplemented method in concrete class: send")

    def save(self):
        """
        Save the whole project on disk.

        :rtype: None
        """
        raise Exception("Unimplemented method in concrete class: save")

    def content(self):
        """
        Save the whole project on disk.

        :rtype: None
        """
        raise Exception("Unimplemented method in concrete class: content")

    def getExternalFiles(self):
        """
        :returns: The list of files that this project may read and write.
        :rtype: str list
        """
        raise Exception("Unimplemented method in concrete class: getExtenalFiles")

    def reload(self):
        """
        Reload the whole project from disk.

        :rtype: None
        """
        raise Exception("Unimplemented method in concrete class: reload")

    @staticmethod
    def isConfigured(project_class, conf, data):
        """
        :param dict conf: Offlate configuration
        :returns: Whether the configuration is sufficient for this project (system)
        :rtype: bool
        """
        for spec in project_class.getSystemConfigSpec():
            if not spec.isConfigured(conf, data):
                return False
        return True

    @staticmethod
    def getSystemConfigSpec(data=None):
        """
        Each system has two kinds of configuration: a global configuration used
        by any instance of the same system (eg: an API key for a given system),
        and a per-project configuration (eg: the name, a URL, ...)

        This returns the specification of the first kind of configuration:
        the global configuration for the system.

        :returns: The specification for the configuration of this type of system.
        """
        raise Exception("Unimplemented method in concrete class: getSystemConfigSpec")

    @staticmethod
    def getProjectConfigSpec():
        """
        Each system has two kinds of configuration: a global configuration used
        by any instance of the same system (eg: an API key for a given system),
        and a per-project configuration (eg: the name, a URL, ...)

        This returns the specification of the second kind of configuration:
        the per-project configuration.

        :returns: The specification for the configuration of projects of this
            type.
        """
        raise Exception("Unimplemented method in concrete class: getProjectConfigSpec")
