#   Copyright (c) 2019 Julien Lepiller <julien@lepiller.eu>
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU Affero General Public License as
#   published by the Free Software Foundation, either version 3 of the
#   License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU Affero General Public License for more details.
#
#   You should have received a copy of the GNU Affero General Public License
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.
####

import datetime
import os
import re
import sys

from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *

from translate.lang.data import languages, tr_lang

from ...systems.list import systems
from ...core.manager import ProjectManager
from ...core.config import *
from ..settingsedit import ListSettingsEdit, RowSettingsEdit, SettingsLineEdit

class SettingsWindow(QMainWindow):
    def __init__(self, manager):
        super().__init__()
        self.manager = manager
        self.initUI()

    def initUI(self):
        center = QDesktopWidget().availableGeometry().center()
        self.setGeometry(center.x()-200, center.y()-400, 400, 800)
        self.setWindowTitle(self.tr('Offlate Settings'))
        self.welcomeWidget = SettingsWidget(self, self.manager)
        self.setCentralWidget(self.welcomeWidget)

    def show(self):
        self.welcomeWidget.show()
        super().show()

class SystemSettingsWidget(QWidget):
    def __init__(self, manager, system = -1, data=None, parent=None):
        super().__init__(parent)
        self.system = systems[system]
        self.manager = manager
        self.data = data
        self.initUI()

    def initUI(self):
        vbox = QVBoxLayout()
        name = self.system['name']
        key = self.system['key']
        system = self.system['system']
        spec = system.getSystemConfigSpec(self.data)

        self.conf = self.manager.settings.conf
        if not key in self.conf:
            self.conf[key] = {}
        self.widgets = {key: {}}

        formBox = QGroupBox(self.tr(name))
        formLayout = QFormLayout()

        for s in spec:
            widget = None
            if isinstance(s, StringConfigSpec):
                widget = SettingsLineEdit()
            elif isinstance(s, ListConfigSpec):
                widget = ListSettingsEdit(s)
            elif isinstance(s, RowConfigSpec):
                widget = RowSettingsEdit(s)
            else:
                raise Exception('Unknown spec type ' + str(s))

            try:
                widget.setContent(self.conf[key][s.key])
            except Exception:
                pass
            widget.textChanged.connect(self.update)

            formLayout.addRow(QLabel(self.tr(s.name)), widget)
            label = QLabel(self.tr(s.description))
            label.setWordWrap(True)
            label.setOpenExternalLinks(True)
            formLayout.addRow(label)
            self.widgets[key][s.key] = widget

        formBox.setLayout(formLayout)
        vbox.addWidget(formBox)
        self.setLayout(vbox)

    def update(self):
        key = self.system['key']
        system = self.system['system']
        spec = system.getSystemConfigSpec(self.data)
        for s in spec:
            widget = self.widgets[key][s.key]
            self.conf[key][s.key] = widget.content()

class SystemSettingsWindow(QDialog):
    def __init__(self, manager, system = -1, data=None, parent = None):
        super().__init__(parent)
        self.widget = SystemSettingsWidget(manager, system, data, self)

        buttons = QHBoxLayout()
        buttons.addStretch(1)
        okbutton = QPushButton(self.tr("OK"))
        buttons.addWidget(okbutton)
        okbutton.clicked.connect(self.close)

        vbox = QVBoxLayout()
        vbox.addWidget(self.widget)
        vbox.addLayout(buttons)
        self.setLayout(vbox)

    def data(self):
        return self.widget.data

class CopyrightSettingsWidget(QWidget):
    def __init__(self, name="", email="", parent=None):
        super().__init__(parent)
        self.name = name
        self.email = email
        self.initUI()

    def initUI(self):
        layout = QVBoxLayout()

        explainText = QLabel(self)
        explainText.setText(self.tr("In some cases, we need to add a copyright \
line for you in the translation files we send upstream. Here, you can configure \
how you want the copyright line to look like. This information will most likely \
become public after your first contribution."))
        explainText.setWordWrap(True)
        layout.addWidget(explainText)

        form = QFormLayout()
        self.nameWidget = QLineEdit()
        self.nameWidget.setText(self.name)
        self.nameWidget.setPlaceholderText(self.tr("John Doe"))
        self.emailWidget = QLineEdit()
        self.emailWidget.setText(self.email)
        self.emailWidget.setPlaceholderText(self.tr("john@doe.me"))
        form.addRow(QLabel("Name:"), self.nameWidget)
        form.addRow(QLabel("Email:"), self.emailWidget)
        layout.addLayout(form)

        layout.addSpacing(15)

        previewLabel = QLabel()
        previewLabel.setText(self.tr("Here is how your copyright line will look like:"))
        layout.addWidget(previewLabel)

        layout.addSpacing(9)

        self.previewLine = QLabel()
        self.previewLine.setTextFormat(Qt.RichText)
        self.previewLine.setWordWrap(True)
        self.previewLine.setAlignment(Qt.AlignCenter)
        pal = QPalette()
        pal.setColor(QPalette.Window, Qt.white)
        self.previewLine.setAutoFillBackground(True)
        self.previewLine.setMargin(9)
        self.previewLine.setPalette(pal)
        layout.addWidget(self.previewLine)
        self.updatePreview()
        self.nameWidget.textChanged.connect(self.updatePreview)
        self.emailWidget.textChanged.connect(self.updatePreview)

        self.setLayout(layout)

    def updatePreview(self, _ignore=None):
        currentDateTime = datetime.datetime.now()
        date = currentDateTime.date()
        year = date.strftime("%Y")
        name = self.nameWidget.text()
        nameHolder = "<i>{}</i>".format(self.tr("John Doe"))
        email = self.emailWidget.text()
        emailHolder = "<i>{}</i>".format(self.tr("john@doe.me"))
        self.previewLine.setText(self.tr("Copyright &copy; {} {} &lt;{}&gt;").format(year,
            name if name != "" else nameHolder, email if email != "" else emailHolder))

class LangSettingsWidget(QWidget):
    changed = pyqtSignal(str)

    def __init__(self, lang="", parent=None):
        super().__init__(parent)
        self.lang = lang
        self.initUI()

    def initUI(self):
        layout = QVBoxLayout()

        explainText = QLabel(self)
        explainText.setText(self.tr("Which language will you most frequently \
translate projects into? This setting can be overriden in each individual \
projects."))
        explainText.setWordWrap(True)
        layout.addWidget(explainText)

        self.languageCombo = QComboBox()
        self.languageCombo.addItem(self.tr("None"), userData="")
        i = 1
        langIndex = 0
        for lang in languages:
            langtag = lang
            lang = languages[lang]
            langname = lang[0]
            self.languageCombo.addItem(tr_lang()(langname), userData=langtag)
            if langtag == self.lang:
                langIndex = i
            i += 1
        self.languageCombo.setCurrentIndex(langIndex)
        self.languageCombo.currentIndexChanged.connect(self.comboChanged)
        layout.addWidget(self.languageCombo)

        layout.addSpacing(15)

        warnText = QLabel(self)
        warnText.setText(self.tr("Note that you should only translate into \
your native language or to a language you are extremely familiar with, to \
avoid weird or nonsensical translations."))
        warnText.setWordWrap(True)
        layout.addWidget(warnText)

        # TODO: Add information about the selected language, to redirect users
        # to translation teams, resources, etc

        # TODO: Allow users to select an unlisted language, by providing
        # language codes.

        self.setLayout(layout)

    def comboChanged(self, index):
        self.changed.emit(self.languageCombo.itemData(index))

    def getLang(self):
        return self.languageCombo.itemData(self.languageCombo.currentIndex())

class SettingsWidget(QWidget):
    def __init__(self, parent=None, manager=None):
        super().__init__(parent)
        self.parent = parent
        self.manager = manager

    def show(self):
        self.initUI()
        super().show()

    def initUI(self):
        vbox = QVBoxLayout()

        self.copyright = CopyrightSettingsWidget(self.manager.getConf(['Generic', 'name']),
                self.manager.getConf(['Generic', 'email']))
        vbox.addWidget(self.copyright)

        self.lang = LangSettingsWidget(self.manager.getConf(['Generic', 'lang']))
        vbox.addWidget(self.lang)

        systemsbox = QGridLayout()
        x = 0
        y = 0
        widthScale = QApplication.desktop().logicalDpiX() / 96
        heightScale = QApplication.desktop().logicalDpiY() / 96
        i = 0
        for system in systems:
            name = system['name']
            key = system['key']
            system = system['system']
            systembox = QVBoxLayout()
            filename = os.path.dirname(__file__) + '/../data/' + key + '.png'
            icon = QPixmap(filename)
            iconLabel = QLabel(self)
            iconLabel.setPixmap(icon.scaled(int(48*widthScale), int(48*heightScale),
                Qt.KeepAspectRatio, Qt.SmoothTransformation))
            iconLabel.setAlignment(Qt.AlignCenter)

            button = QPushButton(self.tr("Configure me"))
            button.clicked.connect(self.getSettingsOpener(i))

            nameLabel = QLabel(self)
            nameLabel.setText(name)
            nameLabel.setAlignment(Qt.AlignCenter)
            systembox.addWidget(iconLabel)
            systembox.addWidget(nameLabel)
            systembox.addWidget(button)

            systemsbox.addLayout(systembox, y, x)
            x += 1
            if x == 2:
                x = 0
                y += 1
            i += 1
        vbox.addLayout(systemsbox)

        vbox.addStretch(1)
        buttonbox = QHBoxLayout()
        buttonbox.addStretch(1)
        doneButton = QPushButton(self.tr("Done!"))
        doneButton.clicked.connect(self.done)
        buttonbox.addWidget(doneButton)
        vbox.addLayout(buttonbox)

        self.setLayout(vbox)

    def done(self):
        name = self.copyright.nameWidget.text()
        email = self.copyright.emailWidget.text()
        lang = self.lang.getLang()
        if name == None or name == "":
            return
        if email == None or email == "":
            return

        self.manager.updateSettings({'Generic': {'name': name, 'email': email,
            'lang': lang, 'new': 'False'}})
        self.quit()

    def quit(self):
        self.parent.hide()

    def openSettings(self, system):
        w = SystemSettingsWindow(self.manager, system, parent = self)
        w.exec_()

    def getSettingsOpener(self, system):
        return lambda : self.openSettings(system)
