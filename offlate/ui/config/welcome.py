#   Copyright (c) 2019 Julien Lepiller <julien@lepiller.eu>
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU Affero General Public License as
#   published by the Free Software Foundation, either version 3 of the
#   License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU Affero General Public License for more details.
#
#   You should have received a copy of the GNU Affero General Public License
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.
####

import os
import re
import sys

from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *

from ...systems.list import systems
from ...core.manager import ProjectManager
from .settings import *

class WelcomeWindow(QMainWindow):
    def __init__(self, manager):
        super().__init__()
        self.manager = manager
        self.initUI()

    def initUI(self):
        center = QDesktopWidget().availableGeometry().center()
        self.setGeometry(center.x()-150, center.y()-400, 300, 800)
        self.setWindowTitle(self.tr('Welcome to Offlate'))
        self.welcomeWidget = SettingsWidget(self, self.manager, True)
        self.setCentralWidget(self.welcomeWidget)
