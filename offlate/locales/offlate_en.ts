<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS><TS version="2.0" language="en" sourcelanguage="">
<context>
    <name>AboutWindow</name>
    <message>
        <location filename="../ui/about.py" line="45"/>
        <source>Offlate is a translation interface for offline translation of projects using online platforms. Offlate is free software, you can redistribute it under the GPL v3 license or any later version.</source>
        <translation>Offlate is a translation interface for offline translation of projects using online platforms. Offlate is free software, you can redistribute it under the GPL v3 license or any later version.</translation>
    </message>
    <message>
        <location filename="../ui/about.py" line="49"/>
        <source>Copyright (C) 2018, 2019 Julien Lepiller</source>
        <translation type="obsolete">Copyright © 2018, 2019 Julien Lepiller</translation>
    </message>
    <message>
        <location filename="../ui/about.py" line="53"/>
        <source>Report an issue</source>
        <translation>Report an issue</translation>
    </message>
    <message>
        <location filename="../ui/about.py" line="54"/>
        <source>Close this window</source>
        <translation>Close this window</translation>
    </message>
    <message>
        <location filename="../ui/about.py" line="51"/>
        <source>Copyright (C) 2018-2021 Julien Lepiller</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>AdvancedProjectWidget</name>
    <message>
        <location filename="../ui/new.py" line="94"/>
        <source>Project information</source>
        <translation type="unfinished">Project information</translation>
    </message>
    <message>
        <location filename="../ui/new.py" line="102"/>
        <source>Name:</source>
        <translation type="unfinished">Name:</translation>
    </message>
    <message>
        <location filename="../ui/new.py" line="103"/>
        <source>Target Language:</source>
        <translation type="unfinished">Target Language:</translation>
    </message>
</context>
<context>
    <name>CopyrightSettingsWidget</name>
    <message>
        <location filename="../ui/config/settings.py" line="139"/>
        <source>In some cases, we need to add a copyright line for you in the translation files we send upstream. Here, you can configure how you want the copyright line to look like. This information will most likely become public after your first contribution.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/config/settings.py" line="186"/>
        <source>John Doe</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/config/settings.py" line="188"/>
        <source>john@doe.me</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/config/settings.py" line="160"/>
        <source>Here is how your copyright line will look like:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/config/settings.py" line="189"/>
        <source>Copyright &amp;copy; {} {} &amp;lt;{}&amp;gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>EditorWindow</name>
    <message>
        <location filename="../ui/editor.py" line="551"/>
        <source>Unsupported / Unknown project</source>
        <translation>Unsupported / Unknown project</translation>
    </message>
    <message numerus="yes">
        <location filename="../ui/editor.py" line="573"/>
        <source>{} translated on {} total ({}%).</source>
        <translation>
            <numerusform>{} translated on {} total ({}%).</numerusform>
            <numerusform>{} translated on {} total ({}%).</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../ui/editor.py" line="672"/>
        <source>Exit</source>
        <translation>Exit</translation>
    </message>
    <message>
        <location filename="../ui/editor.py" line="674"/>
        <source>Exit application</source>
        <translation>Exit application</translation>
    </message>
    <message>
        <location filename="../ui/editor.py" line="677"/>
        <source>Save</source>
        <translation>Save</translation>
    </message>
    <message>
        <location filename="../ui/editor.py" line="679"/>
        <source>Save current project</source>
        <translation>Save current project</translation>
    </message>
    <message>
        <location filename="../ui/editor.py" line="682"/>
        <source>New</source>
        <translation>New</translation>
    </message>
    <message>
        <location filename="../ui/editor.py" line="684"/>
        <source>New project</source>
        <translation>New project</translation>
    </message>
    <message>
        <location filename="../ui/editor.py" line="687"/>
        <source>Manage Projects</source>
        <translation>Manage Projects</translation>
    </message>
    <message>
        <location filename="../ui/editor.py" line="690"/>
        <source>Open project manager</source>
        <translation>Open project manager</translation>
    </message>
    <message>
        <location filename="../ui/editor.py" line="693"/>
        <source>Update</source>
        <translation>Update</translation>
    </message>
    <message>
        <location filename="../ui/editor.py" line="695"/>
        <source>Get modifications from upstream</source>
        <translation>Get modifications from upstream</translation>
    </message>
    <message>
        <location filename="../ui/editor.py" line="698"/>
        <source>Close</source>
        <translation>Close</translation>
    </message>
    <message>
        <location filename="../ui/editor.py" line="699"/>
        <source>Close current project</source>
        <translation>Close current project</translation>
    </message>
    <message>
        <location filename="../ui/editor.py" line="702"/>
        <source>Send</source>
        <translation>Send</translation>
    </message>
    <message>
        <location filename="../ui/editor.py" line="704"/>
        <source>Send modifications upstream</source>
        <translation>Send modifications upstream</translation>
    </message>
    <message>
        <location filename="../ui/editor.py" line="707"/>
        <source>Settings</source>
        <translation>Settings</translation>
    </message>
    <message>
        <location filename="../ui/editor.py" line="709"/>
        <source>Set parameters</source>
        <translation>Set parameters</translation>
    </message>
    <message>
        <location filename="../ui/editor.py" line="722"/>
        <source>Show Translated</source>
        <translation>Show Translated</translation>
    </message>
    <message>
        <location filename="../ui/editor.py" line="725"/>
        <source>Show Fuzzy</source>
        <translation>Show Fuzzy</translation>
    </message>
    <message>
        <location filename="../ui/editor.py" line="728"/>
        <source>Show Empty Translation</source>
        <translation>Show Empty Translation</translation>
    </message>
    <message>
        <location filename="../ui/editor.py" line="731"/>
        <source>Use a monospace font</source>
        <translation>Use a monospace font</translation>
    </message>
    <message>
        <location filename="../ui/editor.py" line="756"/>
        <source>Open</source>
        <translation>Open</translation>
    </message>
    <message>
        <location filename="../ui/editor.py" line="760"/>
        <source>&amp;File</source>
        <translation>&amp;File</translation>
    </message>
    <message>
        <location filename="../ui/editor.py" line="768"/>
        <source>&amp;Project</source>
        <translation>&amp;Project</translation>
    </message>
    <message>
        <location filename="../ui/editor.py" line="775"/>
        <source>&amp;Edit</source>
        <translation>&amp;Edit</translation>
    </message>
    <message>
        <location filename="../ui/editor.py" line="780"/>
        <source>&amp;View</source>
        <translation>&amp;View</translation>
    </message>
    <message>
        <location filename="../ui/editor.py" line="585"/>
        <source>Uploading {}...</source>
        <translation>Uploading {}…</translation>
    </message>
    <message>
        <location filename="../ui/editor.py" line="592"/>
        <source>Finished uploading {}!</source>
        <translation>Finished uploading {}!</translation>
    </message>
    <message>
        <location filename="../ui/editor.py" line="595"/>
        <source>Updating {}...</source>
        <translation>Updating {}…</translation>
    </message>
    <message>
        <location filename="../ui/editor.py" line="607"/>
        <source>Finished updating {}!</source>
        <translation>Finished updating {}!</translation>
    </message>
    <message>
        <location filename="../ui/editor.py" line="712"/>
        <source>Search</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/editor.py" line="714"/>
        <source>Search in the document</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/editor.py" line="717"/>
        <source>Replace</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/editor.py" line="719"/>
        <source>Replace content in the document</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>GitlabEdit</name>
    <message>
        <location filename="../ui/gitlabedit.py" line="59"/>
        <source>server</source>
        <translation>server</translation>
    </message>
    <message>
        <location filename="../ui/gitlabedit.py" line="61"/>
        <source>token</source>
        <translation>token</translation>
    </message>
    <message>
        <location filename="../ui/gitlabedit.py" line="62"/>
        <source>Add</source>
        <translation>Add</translation>
    </message>
    <message>
        <location filename="../ui/gitlabedit.py" line="64"/>
        <source>Remove</source>
        <translation>Remove</translation>
    </message>
</context>
<context>
    <name>LangSettingsWidget</name>
    <message>
        <location filename="../ui/config/settings.py" line="204"/>
        <source>Which language will you most frequently translate projects into? This setting can be overriden in each individual projects.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/config/settings.py" line="211"/>
        <source>None</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/config/settings.py" line="229"/>
        <source>Note that you should only translate into your native language or to a language you are extremely familiar with, to avoid weird or nonsensical translations.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ListSettingsEdit</name>
    <message>
        <location filename="../ui/settingsedit.py" line="142"/>
        <source>Add</source>
        <translation type="unfinished">Add</translation>
    </message>
    <message>
        <location filename="../ui/settingsedit.py" line="144"/>
        <source>Remove</source>
        <translation type="unfinished">Remove</translation>
    </message>
</context>
<context>
    <name>ListSettingsRowEdit</name>
    <message>
        <location filename="../ui/settingsedit.py" line="53"/>
        <source>OK</source>
        <translation type="unfinished">OK</translation>
    </message>
</context>
<context>
    <name>MultipleLineEdit</name>
    <message>
        <location filename="../ui/multiplelineedit.py" line="59"/>
        <source>Add</source>
        <translation>Add</translation>
    </message>
    <message>
        <location filename="../ui/multiplelineedit.py" line="61"/>
        <source>Remove</source>
        <translation>Remove</translation>
    </message>
</context>
<context>
    <name>NewWindow</name>
    <message>
        <location filename="../ui/new.py" line="57"/>
        <source>Project information</source>
        <translation type="obsolete">Project information</translation>
    </message>
    <message>
        <location filename="../ui/new.py" line="65"/>
        <source>Name:</source>
        <translation type="obsolete">Name:</translation>
    </message>
    <message>
        <location filename="../ui/new.py" line="66"/>
        <source>Target Language:</source>
        <translation type="obsolete">Target Language:</translation>
    </message>
    <message>
        <location filename="../ui/new.py" line="68"/>
        <source>The Translation Project</source>
        <translation type="obsolete">The Translation Project</translation>
    </message>
    <message>
        <location filename="../ui/new.py" line="69"/>
        <source>Transifex</source>
        <translation type="obsolete">Transifex</translation>
    </message>
    <message>
        <location filename="../ui/new.py" line="70"/>
        <source>Gitlab</source>
        <translation type="obsolete">Gitlab</translation>
    </message>
    <message>
        <location filename="../ui/new.py" line="230"/>
        <source>Cancel</source>
        <translation>Cancel</translation>
    </message>
    <message>
        <location filename="../ui/new.py" line="231"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../ui/new.py" line="100"/>
        <source>Organization</source>
        <translation type="obsolete">Organization</translation>
    </message>
    <message>
        <location filename="../ui/new.py" line="122"/>
        <source>repository</source>
        <translation type="obsolete">repository</translation>
    </message>
    <message>
        <location filename="../ui/new.py" line="127"/>
        <source>branch</source>
        <translation type="obsolete">branch</translation>
    </message>
    <message>
        <location filename="../ui/new.py" line="71"/>
        <source>Github</source>
        <translation type="obsolete">Github</translation>
    </message>
</context>
<context>
    <name>PageCopyright</name>
    <message>
        <location filename="../ui/welcome.py" line="98"/>
        <source>Copyright Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/welcome.py" line="99"/>
        <source>Configuring how your copyright is added to files</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PageDownload</name>
    <message>
        <location filename="../ui/welcome.py" line="188"/>
        <source>Project Fetch</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/welcome.py" line="189"/>
        <source>Downloading translation files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/welcome.py" line="192"/>
        <source>We are now attempting to fetch the translation files of your project. This step should be pretty fast.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/welcome.py" line="222"/>
        <source>Finished!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PageHints</name>
    <message>
        <location filename="../ui/welcome.py" line="302"/>
        <source>You can always go back to the initial screen (called the project manager) from the editor, by using &lt;i&gt;file &gt; project manager&lt;/i&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/welcome.py" line="311"/>
        <source>Quickly switch to the next string with &lt;i&gt;Ctrl+Enter&lt;/i&gt;. Use &lt;i&gt;Ctrl+Shift+Enter&lt;/i&gt; to go back to the previous string instead.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/welcome.py" line="320"/>
        <source>With these hints, you are now ready to start working on your project! We will now open the project manager, from which you can add more projects, change your settings, etc. Have fun!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PageLang</name>
    <message>
        <location filename="../ui/welcome.py" line="117"/>
        <source>Language Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/welcome.py" line="118"/>
        <source>Configuring the default language for new projects</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PageProjectSelection</name>
    <message>
        <location filename="../ui/welcome.py" line="140"/>
        <source>First Project</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/welcome.py" line="141"/>
        <source>Choosing a first project</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PageSuccess</name>
    <message>
        <location filename="../ui/welcome.py" line="279"/>
        <source>Congratulations! We&apos;ve just set up your first project! Initial setup is now complete, and you can always change the settings you&apos;ve set here from the Offlate welcome screen. After a few explanations you will be able to work on your project right away!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PageSystemSettings</name>
    <message>
        <location filename="../ui/welcome.py" line="163"/>
        <source>System Configuration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/welcome.py" line="164"/>
        <source>Getting an account on the project&apos;s platform</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PageWelcome</name>
    <message>
        <location filename="../ui/welcome.py" line="81"/>
        <source>Offlate is a tool to help you localise free and open source software. Before you start contributing translations to a project though, there are a few things we need to set up and talk about. Let&apos;s get started!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Project</name>
    <message>
        <location filename="../systems/gitlab.py" line="92"/>
        <source>Repository</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../systems/gitlab.py" line="92"/>
        <source>Full clone URL for the repository</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../systems/github.py" line="89"/>
        <source>https://...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../systems/gitlab.py" line="95"/>
        <source>Branch</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../systems/gitlab.py" line="95"/>
        <source>Name of the branch to translate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../systems/gitlab.py" line="95"/>
        <source>master</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../systems/weblate.py" line="303"/>
        <source>Token</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../systems/github.py" line="98"/>
        <source>You can get a token from &lt;a href=&quot;#&quot;&gt;https://github.com/settings/tokens/new&lt;/a&gt;. You will need at least to grant the public_repo permission.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../systems/gitlab.py" line="105"/>
        <source>Gitlab instance configuration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../systems/gitlab.py" line="105"/>
        <source>You need to configure each Gitlab instance separately, and you haven&apos;t configured the instance at {} yet.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../systems/gitlab.py" line="105"/>
        <source>The token you created from your account. You can create it from &lt;a href=&quot;#&quot;&gt;the Access Tokens tab&lt;/a&gt; in your account settings.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../systems/gitlab.py" line="119"/>
        <source>Configured Gitlab instances</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../systems/gitlab.py" line="119"/>
        <source>You need to create a token for each Gitlab instance you have an account on. You can create a token by logging into your account, going to your settings and in the Access Token page.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../systems/weblate.py" line="303"/>
        <source>Server</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../systems/weblate.py" line="303"/>
        <source>Server name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../systems/weblate.py" line="303"/>
        <source>The token you created from your account</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../systems/tp.py" line="153"/>
        <source>version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../systems/tp.py" line="153"/>
        <source>version of the project (keep empty for latest)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../systems/tp.py" line="159"/>
        <source>Mail server</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../systems/tp.py" line="159"/>
        <source>To send your work to the translation project on your behalf, we need to know the email server you are going to use (usually the part on the right of the `@` in your email address).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../systems/tp.py" line="159"/>
        <source>example.com</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../systems/tp.py" line="164"/>
        <source>Mail user</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../systems/tp.py" line="164"/>
        <source>Username used to connect to your mail server, usually the email address itself, or the part of the left of the `@`.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../systems/tp.py" line="164"/>
        <source>john</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../systems/transifex.py" line="147"/>
        <source>You can get a token from &lt;a href=&quot;#&quot;&gt;https://www.transifex.com/user/settings/api/&lt;/a&gt;</source>
        <translation type="unfinished">You can get a token from &lt;a href=&quot;#&quot;&gt;https://www.transifex.com/user/settings/api/&lt;/a&gt;</translation>
    </message>
    <message>
        <location filename="../systems/transifex.py" line="153"/>
        <source>Organization</source>
        <translation type="unfinished">Organization</translation>
    </message>
    <message>
        <location filename="../systems/transifex.py" line="153"/>
        <source>The organization this project belongs in, on transifex.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../systems/weblate.py" line="285"/>
        <source>Project</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../systems/transifex.py" line="155"/>
        <source>The name of the project on transifex</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../systems/weblate.py" line="282"/>
        <source>Instance</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../systems/weblate.py" line="303"/>
        <source>https://weblate.org</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../systems/weblate.py" line="285"/>
        <source>Name of the project on the instance</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../systems/weblate.py" line="285"/>
        <source>foo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../systems/weblate.py" line="292"/>
        <source>Weblate instance configuration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../systems/weblate.py" line="292"/>
        <source>You need to configure each Weblate instance separately, and you haven&apos;t configured the instance at {} yet.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../systems/weblate.py" line="292"/>
        <source>The token you created from your account. You can create it from &lt;a href=&quot;#&quot;&gt;the API access tab&lt;/a&gt; in your account settings.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../systems/weblate.py" line="303"/>
        <source>fKbStkBgFzIL0UW15sfcJh7kC0BAbcVtV16kblXlM</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../systems/weblate.py" line="303"/>
        <source>Configured Weblate instances</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../systems/weblate.py" line="303"/>
        <source>You need to find a token for each Weblate instance you have an account on. You can create a token by logging into your account, going to your settings and in the API Access page.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../systems/weblate.py" line="282"/>
        <source>URL of the Weblate instance</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../systems/weblate.py" line="282"/>
        <source>https://hosted.weblate.org</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ProjectManagerWidget</name>
    <message>
        <location filename="../ui/manager.py" line="89"/>
        <source>Open</source>
        <translation>Open</translation>
    </message>
    <message>
        <location filename="../ui/manager.py" line="90"/>
        <source>Edit</source>
        <translation>Edit</translation>
    </message>
    <message>
        <location filename="../ui/manager.py" line="91"/>
        <source>Remove</source>
        <translation>Remove</translation>
    </message>
    <message>
        <location filename="../ui/manager.py" line="99"/>
        <source>New Project</source>
        <translation>New Project</translation>
    </message>
    <message>
        <location filename="../ui/manager.py" line="100"/>
        <source>Settings</source>
        <translation>Settings</translation>
    </message>
    <message>
        <location filename="../ui/manager.py" line="101"/>
        <source>About Offlate</source>
        <translation>About Offlate</translation>
    </message>
    <message>
        <location filename="../ui/manager.py" line="102"/>
        <source>Exit</source>
        <translation>Exit</translation>
    </message>
    <message>
        <location filename="../ui/manager.py" line="219"/>
        <source>Fetching project {}...</source>
        <translation>Fetching project {}…</translation>
    </message>
</context>
<context>
    <name>ProjectManagerWindow</name>
    <message>
        <location filename="../ui/manager.py" line="53"/>
        <source>Offlate Project Manager</source>
        <translation>Offlate Project Manager</translation>
    </message>
</context>
<context>
    <name>ProjectView</name>
    <message>
        <location filename="../ui/editor.py" line="159"/>
        <source>Copy</source>
        <translation>Copy</translation>
    </message>
    <message>
        <location filename="../ui/editor.py" line="303"/>
        <source>Singular</source>
        <translation>Singular</translation>
    </message>
    <message>
        <location filename="../ui/editor.py" line="304"/>
        <source>Plural</source>
        <translation>Plural</translation>
    </message>
    <message>
        <location filename="../ui/editor.py" line="155"/>
        <source>Open in external editor</source>
        <translation>Open in external editor</translation>
    </message>
    <message>
        <location filename="../ui/editor.py" line="368"/>
        <source>&lt;b&gt;location&lt;/b&gt;: {0} line {1}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/editor.py" line="370"/>
        <source>&lt;b&gt;comment&lt;/b&gt;: {0}</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SearchWindow</name>
    <message>
        <location filename="../ui/search.py" line="84"/>
        <source>String to search for</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/search.py" line="87"/>
        <source>String to replace into</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/search.py" line="92"/>
        <source>Search</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/search.py" line="93"/>
        <source>Replace</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/search.py" line="102"/>
        <source>Case sensitive</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/search.py" line="103"/>
        <source>Wrap around</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/search.py" line="105"/>
        <source>Match whole word only</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/search.py" line="106"/>
        <source>Search in original text</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/search.py" line="108"/>
        <source>Search in translated text</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/search.py" line="110"/>
        <source>Search in comments</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/search.py" line="122"/>
        <source>Close</source>
        <translation type="unfinished">Close</translation>
    </message>
    <message>
        <location filename="../ui/search.py" line="126"/>
        <source>Replace all</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/search.py" line="128"/>
        <source>Replace one</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/search.py" line="130"/>
        <source>&lt; Previous</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/search.py" line="132"/>
        <source>Next &gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SettingsWidget</name>
    <message>
        <location filename="../ui/config/settings.py" line="287"/>
        <source>Configure me</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/config/settings.py" line="308"/>
        <source>Done!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SettingsWindow</name>
    <message>
        <location filename="../ui/settings.py" line="42"/>
        <source>Cancel</source>
        <translation type="obsolete">Cancel</translation>
    </message>
    <message>
        <location filename="../ui/settings.py" line="43"/>
        <source>OK</source>
        <translation type="obsolete">OK</translation>
    </message>
    <message>
        <location filename="../ui/settings.py" line="56"/>
        <source>Transifex</source>
        <translation type="obsolete">Transifex</translation>
    </message>
    <message>
        <location filename="../ui/settings.py" line="68"/>
        <source>You can get a token from &lt;a href=&quot;#&quot;&gt;https://www.transifex.com/user/settings/api/&lt;/a&gt;</source>
        <translation type="obsolete">You can get a token from &lt;a href=&quot;#&quot;&gt;https://www.transifex.com/user/settings/api/&lt;/a&gt;</translation>
    </message>
    <message>
        <location filename="../ui/settings.py" line="164"/>
        <source>Token:</source>
        <translation type="obsolete">Token:</translation>
    </message>
    <message>
        <location filename="../ui/settings.py" line="86"/>
        <source>Generic Settings</source>
        <translation type="obsolete">Generic Settings</translation>
    </message>
    <message>
        <location filename="../ui/settings.py" line="89"/>
        <source>John Doe &lt;john@doe.me&gt;</source>
        <translation type="obsolete">John Doe &lt;john@doe.me&gt;</translation>
    </message>
    <message>
        <location filename="../ui/settings.py" line="96"/>
        <source>Full Name:</source>
        <translation type="obsolete">Full Name:</translation>
    </message>
    <message>
        <location filename="../ui/settings.py" line="101"/>
        <source>Generic</source>
        <translation type="obsolete">Generic</translation>
    </message>
    <message>
        <location filename="../ui/settings.py" line="108"/>
        <source>Translation Project</source>
        <translation type="obsolete">Translation Project</translation>
    </message>
    <message>
        <location filename="../ui/settings.py" line="129"/>
        <source>Email:</source>
        <translation type="obsolete">Email:</translation>
    </message>
    <message>
        <location filename="../ui/settings.py" line="130"/>
        <source>Server:</source>
        <translation type="obsolete">Server:</translation>
    </message>
    <message>
        <location filename="../ui/settings.py" line="131"/>
        <source>User Name:</source>
        <translation type="obsolete">User Name:</translation>
    </message>
    <message>
        <location filename="../ui/settings.py" line="178"/>
        <source>Gitlab</source>
        <translation type="obsolete">Gitlab</translation>
    </message>
    <message>
        <location filename="../ui/settings.py" line="180"/>
        <source>Add your gitlab account tokens below. You need to create a token for every gitlab server you have an account on. You can create a token by logging into your account, going to your settings and in the Access Token page.</source>
        <translation type="obsolete">Add your gitlab account tokens below. You need to create a token for every gitlab server you have an account on. You can create a token by logging into your account, going to your settings and in the Access Token page.</translation>
    </message>
    <message>
        <location filename="../ui/settings.py" line="147"/>
        <source>Github</source>
        <translation type="obsolete">Github</translation>
    </message>
    <message>
        <location filename="../ui/settings.py" line="159"/>
        <source>You can get a token from &lt;a href=&quot;#&quot;&gt;https://github.com/settings/tokens/new&lt;/a&gt;.             You will need at least to grant the public_repo permission.</source>
        <translation type="obsolete">&lt;a href=&quot;#&quot;&gt;https://github.com/settings/tokens/new&lt;/a&gt;.
You will need at least to grant the public_repo permission.</translation>
    </message>
    <message>
        <location filename="../ui/config/settings.py" line="42"/>
        <source>Offlate Settings</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SpellCheckEdit</name>
    <message>
        <location filename="../ui/spellcheckedit.py" line="47"/>
        <source>Spelling Suggestions</source>
        <translation>Spelling Suggestions</translation>
    </message>
    <message>
        <location filename="../ui/spellcheckedit.py" line="48"/>
        <source>No Suggestions</source>
        <translation>No Suggestions</translation>
    </message>
</context>
<context>
    <name>SystemSettingsWindow</name>
    <message>
        <location filename="../ui/config/settings.py" line="116"/>
        <source>OK</source>
        <translation type="unfinished">OK</translation>
    </message>
</context>
<context>
    <name>WelcomeWindow</name>
    <message>
        <location filename="../ui/config/welcome.py" line="38"/>
        <source>Welcome to Offlate</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>dialog</name>
    <message>
        <location filename="../ui/main.py" line="60"/>
        <source>The app just crashed, please report the following error, with any relevant information (what you did when the app crashed, any external factor that might be relevant, etc.). You can send your report on {}, or by email to {}.

{}
Traceback:
{}</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>self.parent</name>
    <message>
        <location filename="../ui/parallel.py" line="57"/>
        <source>A project with the same name already exists. The new project was not created. You should first remove the same-named project.</source>
        <translation>A project with the same name already exists. The new project was not created. You should first remove the same-named project.</translation>
    </message>
    <message>
        <location filename="../ui/parallel.py" line="61"/>
        <source>Your filesystem contains a same-named directory for your new project. The new project was not created. You should first remove the same-named directory: &quot;{}&quot;.</source>
        <translation>Your filesystem contains a same-named directory for your new project. The new project was not created. You should first remove the same-named directory: “{}”.</translation>
    </message>
    <message>
        <location filename="../ui/parallel.py" line="67"/>
        <source>The project you added uses the {} format, but it is not supported yet by Offlate. You can try to update the application, or if you are on the latest version already, report it as a bug.</source>
        <translation>The project you added uses the {} format, but it is not supported yet by Offlate. You can try to update the application, or if you are on the latest version already, report it as a bug.</translation>
    </message>
    <message>
        <location filename="../ui/parallel.py" line="75"/>
        <source>An unexpected error occured while fetching the project: {}. You should report this as a bug.</source>
        <translation>An unexpected error occured while fetching the project: {}. You should report this as a bug.</translation>
    </message>
    <message>
        <location filename="../ui/parallel.py" line="71"/>
        <source>The project {} you added could not be found in the translation platform you selected. Did you make a typo while entering the name or other parameters?</source>
        <translation>The project {} you added could not be found in the translation platform you selected. Did you make a typo while entering the name or other parameters?</translation>
    </message>
    <message>
        <location filename="../ui/parallel.py" line="48"/>
        <source>This action did not complete correctly. Try again, and if the issue persists, consider sending a bug report at {}, or by email to {} with the following information, and any relevant information (what you were trying to do, other external factors, etc.) We received the following error message: {}.

Traceback:

{}
Try again?</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>self.qd</name>
    <message>
        <location filename="../ui/editor.py" line="55"/>
        <source>Please enter your password:</source>
        <translation>Please enter your password:</translation>
    </message>
    <message>
        <location filename="../ui/editor.py" line="63"/>
        <source>Token for {} not found. Have you added this server to your settings?.</source>
        <translation>Token for {} not found. Have you added this server to your settings?</translation>
    </message>
    <message>
        <location filename="../ui/editor.py" line="73"/>
        <source>Error while creating branch {}.</source>
        <translation>Error while creating branch {}.</translation>
    </message>
</context>
</TS>
