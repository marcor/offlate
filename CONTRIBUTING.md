How to contribute?
==================

Adding a system
---------------

A system is a class in offlate/systems.  It must implement at least these
methods:

```python
__init__(conf, name, lang, data = {})
```

To initialise the object.  `conf` is the global configuration object for this
system.  `name` is the name of the project and `lang` is the language to which
the project is translated. Finally, `data` may contain additional information
on the project.

```python
open(basedir)
```

Implements the opening procedure for the project.  `basedir` is the directory
in which the project files are located.  This method is called at the beginning
of the application.

```python
initialize(basedir)
```

Initialises a new project.  `basedir` is the directory in which the project
files are located.

```python
update(callback)
```

Updates the project by connecting to the translation platform.  `callback` is
a procedure that can be called to resolve conflicts.  This procedure looks like
`callback(string_to_translate, local_translation, remote_translation)` and
returns a translation for the string to translate.  Note that the returned value
might be different from the translations that were passed to it.

```python
send(interface)
```

Sends the current state of the work to the platform, using `interface` as
a way to ask more information to the user.

```python
save()
```

Saves the current state of the work in the project's directory.  This should
be enough to resume work when the application is opened again and `open` is
called.

```python
content()
```

Retuns the content of the translation files, an object containing one field
per project-resource.  If the system doesn't have a notion of resource, the
resource should be called "default". Each resource contains a list of `Entry`
objects.

Adding a format
---------------

A format is represented by a class in offlate/formats.  It must implement at
least these methods:

```python
init(files, conf)
```

Initialise the object.  `files` is a variable whose meaning depends on the
format.  This variable is most likely an dictionnary containing a few fields
whose values are filenames.  `conf` is a configuration dictionnary for the
format.

```python
content()
```

Returns a list of Entry objects corresponding to the content of the files.

```python
save()
```

Saves the list of Entry.

```python
merge(oldformat, callback)
```

Merge the file in the old file. The callback function is the one passed to
`System.update`.
